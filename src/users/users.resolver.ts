import {Args, Query, Resolver, ResolveReference} from '@nestjs/graphql';
import {UsersService} from "./users.service";
import {User} from "../models/graphql";

@Resolver('User')
export class UsersResolver {
    constructor(private usersService: UsersService) {}

    @Query()
    getUser(@Args('id') id: string): User {
        return this.usersService.findById(id);
    }

    @ResolveReference()
    resolveReference(reference: { __typename: string; id: string }) {
        return this.usersService.findById(reference.id);
    }
}
