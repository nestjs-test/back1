import {AppController} from './app.controller';
import {AppService} from './app.service';
import {UsersService} from './users/users.service';
import {UsersResolver} from './users/users.resolver';
import {join} from 'path';
import {GraphQLFederationModule} from "@nestjs/graphql";
import {Module} from "@nestjs/common";

@Module({
    imports: [
        GraphQLFederationModule.forRoot({
            debug: true,
            playground: true,
            typePaths: ['**/*.graphql'],
            definitions: {
                path: join(process.cwd(), 'src/models/graphql.ts'),
            },
        }),
    ],
    controllers: [AppController],
    providers: [AppService, UsersService, UsersResolver],
})
export class AppModule {
}
